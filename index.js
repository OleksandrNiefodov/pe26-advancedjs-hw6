'use strict'

/*
Теоретичне питання
Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript

- Асинхронність в Javascript означає, що код може виконуватися без блокування інших операцій або на їх очікування. Це можливо завдяки тому, що Javascript використовує однопоточну модель, де код виконується по одному рядку.

Завдання
Написати програму "Я тебе знайду по IP"

Технічні вимоги:
Створити просту HTML-сторінку з кнопкою Знайти по IP.
Натиснувши кнопку - надіслати AJAX запит за адресою https://api.ipify.org/?format=json, отримати звідти IP адресу клієнта.
Дізнавшись IP адресу, надіслати запит на сервіс https://ip-api.com/ та отримати інформацію про фізичну адресу.
під кнопкою вивести на сторінку інформацію, отриману з останнього запиту – континент, країна, регіон, місто, район.
Усі запити на сервер необхідно виконати за допомогою async await.
*/

const findIpButton = document.getElementById('find-ip-button');

findIpButton.addEventListener('click', async () => {
    try {
        const ipResponse = await axios.get('https://api.ipify.org/?format=json');
        const ip = ipResponse.data.ip;

        const locationResponse = await axios.get(`http://ip-api.com/json/${ip}`);
        const locationData = locationResponse.data

        const countryResponse = await axios.get(`http://api.geonames.org/countryInfoJSON?country=${locationData.countryCode}&username=brifle`);
        const continent = countryResponse.data.geonames[0].continentName;

        const responseContainer = document.getElementById('ip-info-container');
        responseContainer.innerHTML = `
            <p>Continent: ${continent}</p>
            <p>Country: ${locationData.country}</p>
            <p>Region: ${locationData.regionName}</p>
            <p>City: ${locationData.city}</p>
        `;
    } catch (error) {
        console.error(error);
    }
});




